#include<stdio.h>

int tab[8][8], mc[8] = {0, 1, 1, 1, 0, -1, -1, -1}, ml[8] = {-1, -1, 0, 1, 1, 1, 0, -1};

int valido(int i, int j){
    if((i < 0)||(j < 0)) return 0;
    if((i >= 8)||(j >= 8)) return 0;
    return 1;
}

int podejogar(int tabuleiro[8][8], int cor, int l, int c){
    int l_, c_;
    if(tabuleiro[l][c] != 0) return 0;
    for(int  k = 0; k < 8; k++){
        l_ = l + ml[k];
        c_ = c + mc[k];
        if(valido(l_, c_)){
            if(tabuleiro[l_][c_] == (-1)*cor){
                l_ += ml[k];
                c_ += mc[k];
                while(valido(l_, c_)){
                    if(tabuleiro[l_][c_] == cor) return 1;
                    else if(tabuleiro[l_][c_] == 0) break;
                    l_ += ml[k];
                    c_ += mc[k];
                }
            }
            else{
                continue;
            }
        }
    }
    return 0;
}

void joga(int tabuleiro[8][8], int cor, int l, int c){
    int l_, c_;
    tabuleiro[l][c] = cor;
    for(int k = 0; k < 8; k++){
        l_ = l + ml[k];
        c_ = c + mc[k];
        if(valido(l_, c_)){
            if(tabuleiro[l_][c_] == (-1)*cor){
                l_ += ml[k];
                c_ += mc[k];
                while(valido(l_, c_)){
                    if(tabuleiro[l_][c_] == cor){
                        int w = (k + 4)%8;
                        l_ += ml[w];
                        c_ += mc[w];
                        while((c_ != c)||(l_ != l)){
                            tabuleiro[l_][c_] = cor;
                            l_ += ml[w];
                            c_ += mc[w];
                        }
                        l_ = c_ = 20;
                    }
                    else if(tabuleiro[l_][c_] == 0) break;
                    else{
                        l_ += ml[k];
                        c_ += mc[k];
                    }
                }
            }
            else{
                continue;
            }
        }
    }
}

void imprime_tabuleiro(int tabuleiro[8][8]){
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            printf("%2d ", tabuleiro[i][j]);
        }
        printf("\n");
    }
    return;
}

void usuariojoga(int tabuleiro[8][8], int cor){
    int l, c;
    printf("Digite a linha onde deseja jogar:");
    scanf("%d", &l);
    printf("Digite a coluna onde deseja jogar:");
    scanf("%d", &c);
    if(podejogar(tabuleiro, cor, l, c)){
        joga(tabuleiro, cor, l, c);
    }
    else{
        printf("Jogada invalida, você perdeu!\n\n");
        return;
    }
}


void escolhejogada(int tabuleiro[8][8], int cor, int *linha, int *coluna){
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            if(podejogar(tabuleiro,cor, i, j)){
                *linha = i;
                *coluna = j;
                return;
            }
        }
    }
    return;
}

int exite_jogada_valida(int tabuleiro[8][8], int cor){
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            if(podejogar(tabuleiro, cor, i, j)) return 1;
        }
    }
    return 0;
}

int conta_pontos(int tabuleiro[8][8]){
    int qt_brancas = 0, qt_pretas = 0;
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            if(tabuleiro[i][j] == 1){
                qt_brancas++;
            }
            else if(tabuleiro[i][j] == -1){
                qt_pretas++;
            }
        }
    }

    if(qt_pretas > qt_brancas){
        return -1;
    }
    else if(qt_brancas > qt_pretas){
        return 1;
    }
    else{
        return 0;
    }
}

int main(){
    char c;
    int continua = 1, linha = 0, coluna = 0, resultado;
    printf("                             Escolha sua cor\n");
    printf("(Digite 'b' para jogar com as brancas ou 'p' para jogar com as pretas):\n");
    scanf("%c", &c);;
    for(int i = 0; i < 8; i++){
        for(int  j = 0; j < 8; j++){
            tab[i][j] = 0;
        }
    }
    tab[3][3] = tab[4][4] = 1;
    tab[3][4] = tab[4][3] = -1;

    if(c == 'p'){
        printf("Você escolheu as pretas!\n\n");
        printf("Tabuleiro inicialmente: \n");
        imprime_tabuleiro(tab);
        while(exite_jogada_valida(tab, -1) || exite_jogada_valida(tab, 1)){
            if(exite_jogada_valida(tab, -1)){
                usuariojoga(tab, -1);
                printf("\nTabuleiro após sua jogada:\n\n");
                imprime_tabuleiro(tab);
            }
            if(exite_jogada_valida(tab, 1)){
                escolhejogada(tab, 1, &linha, &coluna);
                joga(tab, 1, linha, coluna);
                printf("\nTabuleiro após jogada do computador:\n\n");
                imprime_tabuleiro(tab);
            }
        }


        resultado = conta_pontos(tab);

        if(resultado == 1){
            printf("\n\nVocê perdeu!\n\n");
        }
        else if(resultado == -1){
            printf("\n\nVocê ganhou!\n\n");
        }
        else{
            printf("\n\nEmpate!\n\n");
        }
    }
    else if(c == 'b'){
        printf("Você escolheu as brancas!\n\n");
        printf("Tabuleiro inicialmente: \n");
        imprime_tabuleiro(tab);
        while(exite_jogada_valida(tab, -1) || exite_jogada_valida(tab, 1)){
            if(exite_jogada_valida(tab, -1)){
                escolhejogada(tab, -1, &linha, &coluna);
                joga(tab, -1, linha, coluna);
                printf("\nTabuleiro após jogada do computador:\n\n");
                imprime_tabuleiro(tab);
            }
            if(exite_jogada_valida(tab, 1)){
                usuariojoga(tab, 1);
                printf("\nTabuleiro após sua jogada:\n\n");
                imprime_tabuleiro(tab);
            }
        }
        
        resultado = conta_pontos(tab);

        if(resultado == -1){
            printf("\n\nVocê perdeu!\n\n");
        }
        else if(resultado == 1){
            printf("\n\nVocê ganhou!\n\n");
        }
        else{
            printf("\n\nEmpate!\n\n");
        }
    }
    else{
        printf("\n\nEscolha invalida");
        printf("\nVocê perdeu!\n\n");
    }


    return 0;
}